// <copyright file="GPGSIds.cs" company="Google Inc.">
// Copyright (C) 2015 Google Inc. All Rights Reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//    limitations under the License.
// </copyright>

///
/// This file is automatically generated DO NOT EDIT!
///
/// These are the constants defined in the Play Games Console for Game Services
/// Resources.
///


public static class GPS
{
        public const string achievement____20 = "CgkIppGZwo0NEAIQCQ"; // <GPGSID>
        public const string achievement_11 = "CgkIppGZwo0NEAIQDA"; // <GPGSID>
        public const string achievement_13 = "CgkIppGZwo0NEAIQDg"; // <GPGSID>
        public const string achievement_10 = "CgkIppGZwo0NEAIQCw"; // <GPGSID>
        public const string achievement_12 = "CgkIppGZwo0NEAIQDQ"; // <GPGSID>
        public const string achievement_15 = "CgkIppGZwo0NEAIQEA"; // <GPGSID>
        public const string achievement_2 = "CgkIppGZwo0NEAIQAg"; // <GPGSID>
        public const string achievement_3 = "CgkIppGZwo0NEAIQAw"; // <GPGSID>
        public const string achievement = "CgkIppGZwo0NEAIQAQ"; // <GPGSID>
        public const string achievement_6 = "CgkIppGZwo0NEAIQBg"; // <GPGSID>
        public const string achievement_7 = "CgkIppGZwo0NEAIQBw"; // <GPGSID>
        public const string achievement_4 = "CgkIppGZwo0NEAIQBA"; // <GPGSID>
        public const string achievement_5 = "CgkIppGZwo0NEAIQBQ"; // <GPGSID>
        public const string achievement_8 = "CgkIppGZwo0NEAIQCA"; // <GPGSID>
        public const string achievement_9 = "CgkIppGZwo0NEAIQCg"; // <GPGSID>
        public const string achievement_14 = "CgkIppGZwo0NEAIQDw"; // <GPGSID>
        public const string leaderboard_leader_board = "CgkIppGZwo0NEAIQEQ"; // <GPGSID>

}

