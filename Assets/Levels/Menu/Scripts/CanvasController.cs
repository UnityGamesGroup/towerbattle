﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CanvasController : MonoBehaviour
{
    public Canvas mainMenu;
    public Canvas achievementMenu;
    public Canvas settingMenu;
    public Canvas startMenu;
    public Canvas clickEnergyGame;
    public Canvas goToTowerGame;

    public Button btnBack;
    public Image logo;
    private int key;

    void Start()
    {
        mainMenu.GetComponent<Canvas>().enabled = true;
        achievementMenu.GetComponent<Canvas>().enabled = false;
        settingMenu.GetComponent<Canvas>().enabled = false;
        startMenu.GetComponent<Canvas>().enabled = false;
        clickEnergyGame.GetComponent<Canvas>().enabled = false;
        goToTowerGame.GetComponent<Canvas>().enabled = false;
        btnBack.gameObject.SetActive(false);
    }

    public void chengeCanvas(int mode)
    {
        mainMenu.GetComponent<Canvas>().enabled = true;
        settingMenu.GetComponent<Canvas>().enabled = false;
        startMenu.GetComponent<Canvas>().enabled = false;
        clickEnergyGame.GetComponent<Canvas>().enabled = false;
        goToTowerGame.GetComponent<Canvas>().enabled = false;
        achievementMenu.GetComponent<Canvas>().enabled = false;
        btnBack.gameObject.SetActive(false);
        logo.gameObject.SetActive(true);

        if (mode == 1)
        {
            mainMenu.GetComponent<Canvas>().enabled = true;
            settingMenu.GetComponent<Canvas>().enabled = false;
            startMenu.GetComponent<Canvas>().enabled = false;
            clickEnergyGame.GetComponent<Canvas>().enabled = false;
            goToTowerGame.GetComponent<Canvas>().enabled = false;
            achievementMenu.GetComponent<Canvas>().enabled = false;
            btnBack.gameObject.SetActive(false);
            logo.gameObject.SetActive(true);
            key = 0;
        }
        else if (mode ==2)
        {
            mainMenu.GetComponent<Canvas>().enabled = false;
            settingMenu.GetComponent<Canvas>().enabled = true;
            startMenu.GetComponent<Canvas>().enabled = false;
            clickEnergyGame.GetComponent<Canvas>().enabled = false;
            goToTowerGame.GetComponent<Canvas>().enabled = false;
            achievementMenu.GetComponent<Canvas>().enabled = false;
            btnBack.gameObject.SetActive(true);
            logo.gameObject.SetActive(false);
            key = 1;
        }
        else if (mode == 3)
        {
            mainMenu.GetComponent<Canvas>().enabled = false;
            settingMenu.GetComponent<Canvas>().enabled = false;
            startMenu.GetComponent<Canvas>().enabled = true;
            clickEnergyGame.GetComponent<Canvas>().enabled = false;
            goToTowerGame.GetComponent<Canvas>().enabled = false;
            achievementMenu.GetComponent<Canvas>().enabled = false;
            btnBack.gameObject.SetActive(true);
            logo.gameObject.SetActive(false);
            key = 1;
        }
        else if (mode == 4)
        {
            mainMenu.GetComponent<Canvas>().enabled = false;
            settingMenu.GetComponent<Canvas>().enabled = false;
            startMenu.GetComponent<Canvas>().enabled = false;
            clickEnergyGame.GetComponent<Canvas>().enabled = true;
            goToTowerGame.GetComponent<Canvas>().enabled = false;
            achievementMenu.GetComponent<Canvas>().enabled = false;
            btnBack.gameObject.SetActive(true);
            logo.gameObject.SetActive(false);
            key = 0;
        }
        else if (mode == 5)
        {
            mainMenu.GetComponent<Canvas>().enabled = false;
            settingMenu.GetComponent<Canvas>().enabled = false;
            startMenu.GetComponent<Canvas>().enabled = false;
            clickEnergyGame.GetComponent<Canvas>().enabled = false;
            goToTowerGame.GetComponent<Canvas>().enabled = true;
            achievementMenu.GetComponent<Canvas>().enabled = false;
            btnBack.gameObject.SetActive(true);
            logo.gameObject.SetActive(false);
            key = 0;
        }
        else if (mode == 6)
        {
            mainMenu.GetComponent<Canvas>().enabled = false;
            settingMenu.GetComponent<Canvas>().enabled = false;
            startMenu.GetComponent<Canvas>().enabled = false;
            clickEnergyGame.GetComponent<Canvas>().enabled = false;
            goToTowerGame.GetComponent<Canvas>().enabled = false;
            achievementMenu.GetComponent<Canvas>().enabled = true;
            btnBack.gameObject.SetActive(true);
            logo.gameObject.SetActive(false);
            key = 1;
        }
    }

    public void btnBackCanvas()
    {
        if (key == 1)
        {
            chengeCanvas(1);
        } else
        {
            chengeCanvas(3);
        }
    }


    public void btnExit()
    {
        Application.Quit();
    }
}
