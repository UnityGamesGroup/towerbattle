﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;

public class ClickController : MonoBehaviour
{
    
    public Sprite[] clickChangeColor;

    public Button btnClickLeft, btnClickRight;

    //public GameObject[] pnlProgress;
    public Text txtcountCoints, txtlevelCountText, txtlevelNum, txtReward;
    public Slider sliderLevelValue, sliderValueBoost;

    public GameObject pnlInfo;

    private int value, boost = 0;
    public AudioSource clip;

    // Start is called before the first frame update
    void Start()
    {
        sliderLevelValue.value = Global.levelValue;
        txtlevelNum.text = Global.level.ToString();
        txtlevelCountText.text = Global.levelValue.ToString();
        txtcountCoints.text = Global.valueClick.ToString();
        value = Global.valueClick;

    }

    // Update is called once per frame
    void Update()
    {
        if (sliderLevelValue.value == 1000)
        {
            Global.level++;
            Global.valueClick += 100;
            txtlevelNum.text = Global.level.ToString();
            sliderLevelValue.value = 0;
            txtlevelCountText.text = sliderLevelValue.value.ToString();
            rewardController();
            pnlInfo.gameObject.SetActive(true);
            pnlInfo.GetComponent<Animator>().enabled = true;
            pnlInfo.GetComponent<Animator>().Play("newLevelAnim", -1, 0f);
        }

        if (sliderValueBoost.value == 500)
        {
            boost = 1;
        }
        txtcountCoints.text = Global.valueClick.ToString();
    }
    
    public void ClickLeft()
    {  
        if (Input.touchCount < 3)
        {          
            btnClickLeft.GetComponent<Animator>().enabled = true;
            btnClickLeft.GetComponent<Animator>().Play("clickAnim", -1, 0f);           
            btnClickLeft.GetComponent<Image>().sprite = clickChangeColor[UnityEngine.Random.Range(0, clickChangeColor.Length)];
            clickFunc();
        }

    }

    public void ClickRight()
    {

        if (Input.touchCount < 3)
        {
            btnClickRight.GetComponent<Animator>().enabled = true;
            btnClickRight.GetComponent<Animator>().Play("clickAnim", -1, 0f);
            btnClickRight.GetComponent<Image>().sprite = clickChangeColor[UnityEngine.Random.Range(0, clickChangeColor.Length)];
            clickFunc();
        }
    }
    private void clickFunc()
    {
        clip.GetComponent<AudioSource>().Play();
        Global.valueClick += boost;
        txtcountCoints.text = Global.valueClick.ToString();
        sliderLevelValue.value = sliderLevelValue.value + 2;
        txtlevelCountText.text = sliderLevelValue.value.ToString();
        Global.valueClick++;
        sliderValueBoost.value++;
        Global.levelValue = Convert.ToInt32(sliderLevelValue.value);
    }

    public void btnBack()
    {
        sliderValueBoost.value = 0;
    }

    public void LoadTowerLevel(int valueLvl)
    {
        Global.levelTowerValue = valueLvl;
        SceneManager.LoadScene(1);
    }

    public void rewardController()
    {
        if (Global.level <= 10)
        {
            txtReward.text = "+" + 100;
        }
        else if (Global.level > 10 && Global.level <= 20)
        {
            txtReward.text = "+" + 200;
        }
        else if (Global.level > 20 && Global.level <= 30)
        {
            txtReward.text = "+" + 300;
        }
        else if (Global.level > 30 && Global.level <= 40)
        {
            txtReward.text = "+" + 400;
        }
        else if (Global.level > 40)
        {
            txtReward.text = "+" + 500;
        }
    }
    public void startGPService()
    {
        PlayGamesPlatform.DebugLogEnabled = true;
        PlayGamesPlatform.Activate();
        Social.localUser.Authenticate(success =>
        {
            if (success)
            {

            }
            else
            {

            }
        });
    }

}
