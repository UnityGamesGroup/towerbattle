﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Global : MonoBehaviour
{
    public static int level, levelValue, valueClick, levelTowerValue, levelAccess;
    public static string masLevel, masProgress;
    public static int[] levels = new int[40];
    public static int[] progress = new int[16];
    public static int num;
    public static int sound =  1;
}
