﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelController : MonoBehaviour
{
    public GameObject[] pnlLevels;
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < pnlLevels.Length; i++)
        {
            if (Global.levels[i] == 1)
            {
                pnlLevels[i].GetComponent<Button>().interactable = true;
                pnlLevels[i].GetComponentInChildren<Transform>().GetChild(0).GetComponent<Image>().gameObject.SetActive(false);
                Global.levelAccess++;
            }
        }
       
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < pnlLevels.Length; i++)
        {
            if (Global.levels[i] == 1)
            {
                pnlLevels[i].GetComponent<Button>().interactable = true;
                pnlLevels[i].GetComponentInChildren<Transform>().GetChild(0).GetComponent<Image>().gameObject.SetActive(false);
                //Global.levelAccess++;
            }
        }
    }
}
