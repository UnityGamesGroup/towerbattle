﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;


public class ProgressController : MonoBehaviour
{
    public GameObject pnlProgress;
    public GameObject[] lockPanel;

    private const string leaderBoard = "CgkIppGZwo0NEAIQEQ";

    private const string progress1 = "CgkIppGZwo0NEAIQAQ";
    private const string progress2 = "CgkIppGZwo0NEAIQAg";
    private const string progress3 = "CgkIppGZwo0NEAIQAw";
    private const string progress4 = "CgkIppGZwo0NEAIQBA";
    private const string progress5 = "CgkIppGZwo0NEAIQBQ";
    private const string progress6 = "CgkIppGZwo0NEAIQBg";
    private const string progress7 = "CgkIppGZwo0NEAIQBw";
    private const string progress8 = "CgkIppGZwo0NEAIQCA";
    private const string progress9 = "CgkIppGZwo0NEAIQCQ";
    private const string progress10 = "CgkIppGZwo0NEAIQCg";
    private const string progress11 = "CgkIppGZwo0NEAIQCw";
    private const string progress12 = "CgkIppGZwo0NEAIQDA";
    private const string progress13 = "CgkIppGZwo0NEAIQDQ";
    private const string progress14 = "CgkIppGZwo0NEAIQDg";
    private const string progress15 = "CgkIppGZwo0NEAIQDw";
    private const string progress16 = "CgkIppGZwo0NEAIQEA";

    // Start is called before the first frame update
    void Start()
    {
        startGPService();
    }

    // Update is called once per frame
    void Update()
    {
        if (Global.valueClick >= 100 && Global.progress[0] != 1)
        {
            GetTheProgress(progress1);
            Global.progress[0] = 1;
            playAnimProgress();
        }
        if (Global.valueClick >= 200 && Global.progress[1] != 1)
        {
            GetTheProgress(progress2);
            Global.progress[1] = 1;
            playAnimProgress();
        }
        if (Global.valueClick >= 500 && Global.progress[2] != 1)
        {
            GetTheProgress(progress3);
            Global.progress[2] = 1;
            playAnimProgress();
        }
        if (Global.valueClick >= 1000 && Global.progress[3] != 1)
        {
            GetTheProgress(progress4);
            Global.progress[3] = 1;
            playAnimProgress();
        }
        if (Global.valueClick >= 5000 && Global.progress[4] != 1)
        {
            GetTheProgress(progress5);
            Global.progress[4] = 1;
            playAnimProgress();
        }
        if (Global.level == 5 && Global.progress[5] != 1)
        {
            GetTheProgress(progress6);
            Global.progress[5] = 1;
            playAnimProgress();
        }
        if (Global.level == 10 && Global.progress[6] != 1)
        {
            GetTheProgress(progress7);
            Global.progress[6] = 1;
            playAnimProgress();
        }
        if (Global.level == 15 && Global.progress[7] != 1)
        {
            GetTheProgress(progress8);
            Global.progress[7] = 1;
            playAnimProgress();
        }
        if (Global.level == 20 && Global.progress[8] != 1)
        {
            GetTheProgress(progress9);
            Global.progress[8] = 1;
            playAnimProgress();
        }
        if (Global.level == 30 && Global.progress[9] != 1)
        {
            GetTheProgress(progress10);
            Global.progress[9] = 1;
            playAnimProgress();
        }
        if (Global.level == 40 && Global.progress[10] != 1)
        {
            GetTheProgress(progress11);
            Global.progress[10] = 1;
            playAnimProgress();
        }
        if (Global.level == 50 && Global.progress[11] != 1)
        {
            GetTheProgress(progress12);
            Global.progress[11] = 1;
            playAnimProgress();
        }
        if (Global.levelTowerValue == 11 && Global.progress[12] != 1)
        {
            GetTheProgress(progress13);
            Global.progress[12] = 1;
            playAnimProgress();
        }
        if (Global.levelTowerValue == 21 && Global.progress[13] != 1)
        {
            GetTheProgress(progress14);
            Global.progress[13] = 1;
            playAnimProgress();
        }
        if (Global.levelTowerValue == 31 && Global.progress[14] != 1)
        {
            GetTheProgress(progress15);
            Global.progress[14] = 1;
            playAnimProgress();
        }
        if (Global.levelTowerValue == 41 && Global.progress[15] != 1)
        {
            GetTheProgress(progress16);
            Global.progress[15] = 1;
            playAnimProgress();
        }

        for (int i = 0; i < Global.progress.Length; i++)
        {
            if (Global.progress[i] == 1)
            {
                lockPanel[i].SetActive(false);
            }
        }
        Social.ReportScore(Global.levelTowerValue, leaderBoard, (bool success) => { });
    }
    public void playAnimProgress()
    {
        pnlProgress.gameObject.SetActive(true);
        pnlProgress.GetComponent<Animator>().enabled = true;
        pnlProgress.GetComponent<Animator>().Play("progressAnim", -1, 0f);
    }

    public void startGPService()
    {
        PlayGamesPlatform.DebugLogEnabled = true;
        PlayGamesPlatform.Activate();
        Social.localUser.Authenticate(success =>
        {
            if (success)
            {

            }
            else
            {

            }
        });
    }

    public void showLeaderBoard()
    {
        Social.ShowLeaderboardUI();
    }


    public void GetTheProgress(string id)
    {
        Social.ReportProgress(id, 100.0f, (bool success) =>
        {
            if (success)
            {
                print("Получено достижение: " + id);
            }
        });
    }

    public void ExitFromGPS()
    {
        PlayGamesPlatform.Instance.SignOut();
    }
}
