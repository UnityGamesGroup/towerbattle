﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SaveGame : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
        //PlayerPrefs.DeleteAll();
        LoadData();
        if (PlayerPrefs.HasKey("masLevel"))
        {
            string[] level = Global.masLevel.Split(',');
            for (int i = 0; i < Global.levels.Length; i++)
            {
                Global.num = Convert.ToInt32(level[i]);
                Global.levels[i] = Global.num;
            }
        }
        if (PlayerPrefs.HasKey("masProgress"))
        {
            string[] progress = Global.masProgress.Split(',');
            for (int i = 0; i < Global.progress.Length; i++)
            {
                Global.num = Convert.ToInt32(progress[i]);
                Global.progress[i] = Global.num;
            }
        }

    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < Global.levels.Length; i++)
        {
            Global.masLevel += Global.levels[i].ToString() + ",";
        }
        for (int i = 0; i < Global.progress.Length; i++)
        {
            Global.masProgress += Global.progress[i].ToString() + ",";
        }
        SaveData();
        Global.masLevel = null;
        Global.masProgress = null;
    }

    void SaveData()
    {
        //Global.valueClick = 100000;
        PlayerPrefs.SetInt("valueClick", Global.valueClick);
        PlayerPrefs.SetInt("level", Global.level);
        PlayerPrefs.SetInt("levelValue", Global.levelValue);
        //PlayerPrefs.SetInt("levelAccess", Global.levelAccess);
        PlayerPrefs.SetInt("sound", Global.sound);

        PlayerPrefs.SetString("masLevel", Global.masLevel);
        PlayerPrefs.SetString("masProgress", Global.masProgress);

        PlayerPrefs.Save();
    }
    
    void LoadData()
    {
        if (PlayerPrefs.HasKey("valueClick"))
        {
            Global.valueClick = PlayerPrefs.GetInt("valueClick"); 
        }
        if (PlayerPrefs.HasKey("level"))
        {
            Global.level = PlayerPrefs.GetInt("level");
        }
        if (PlayerPrefs.HasKey("levelValue"))
        {
            Global.levelValue = PlayerPrefs.GetInt("levelValue");
        }
        //if (PlayerPrefs.HasKey("levelAccess"))
        //{
        //    Global.levelAccess = PlayerPrefs.GetInt("levelAccess");
        //}
        if (PlayerPrefs.HasKey("sound"))
        {
            Global.sound = PlayerPrefs.GetInt("sound");
        }
        if (PlayerPrefs.HasKey("masLevel"))
        {
            Global.masLevel = PlayerPrefs.GetString("masLevel");
        }
        if (PlayerPrefs.HasKey("masProgress"))
        {
            Global.masProgress = PlayerPrefs.GetString("masProgress");
        }
    }

    public void Reset()
    {
        
    }
}
