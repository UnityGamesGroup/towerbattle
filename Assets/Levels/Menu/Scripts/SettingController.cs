﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingController : MonoBehaviour
{
    public AudioSource[] audioSource;
    public Sprite[] spriteSound;
    public Button imgSound;
    
    // Start is called before the first frame update
    void Start()
    {
        //Global.sound = 1;
        if (Global.sound == 0)
        {
            for (int i = 0; i < audioSource.Length; i++)
            {
                audioSource[i].volume = 0.5f;
            }
            imgSound.GetComponent<Image>().sprite = spriteSound[0];
        }
        if (Global.sound == 1)
        {
            for (int i = 0; i < audioSource.Length; i++)
            {
                audioSource[i].volume = 0.0f;
            }
            imgSound.GetComponent<Image>().sprite = spriteSound[1];
        }
    }

    public void soundController()
    {
        if (Global.sound == 1)
        {
            for (int i = 0; i < audioSource.Length; i++)
            {
                audioSource[i].volume = 0.5f;
            }
            Global.sound = 0;
            imgSound.GetComponent<Image>().sprite = spriteSound[0];
        }
        else if (Global.sound == 0)
        {
            for (int i = 0; i < audioSource.Length; i++)
            {
                audioSource[i].volume = 0.0f; 
            }
            Global.sound = 1;
            imgSound.GetComponent<Image>().sprite = spriteSound[1];
        }
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
