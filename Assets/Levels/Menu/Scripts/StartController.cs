﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartController : MonoBehaviour
{
    public GameObject firstSlide;
    public GameObject firstSlideTower;
    // Start is called before the first frame update
    void Start()
    {
        firstSlide.GetComponent<Scrollbar>().value = 0;
        firstSlideTower.GetComponent<Scrollbar>().value = 1;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
