﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PriceEnergyController : MonoBehaviour
{
    public Text[] txtPrice;
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < Global.levels.Length; i++)
        {
            if (Global.levelTowerValue == 1)
            {
                txtPrice[0].text = (100).ToString();
                txtPrice[1].text = (200).ToString();
                txtPrice[2].text = (300).ToString();
                txtPrice[3].text = (400).ToString();
                break;
            }
            else if (Global.levelTowerValue == i)
            {
                txtPrice[0].text = (100 * i/2).ToString();
                txtPrice[1].text = (200 * i/2).ToString();
                txtPrice[2].text = (300 * i/2).ToString();
                txtPrice[3].text = (400 * i/2).ToString();
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
