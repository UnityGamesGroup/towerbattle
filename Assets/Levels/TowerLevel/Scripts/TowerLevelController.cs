﻿using System.Collections;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TowerLevelController : MonoBehaviour
{
    public Text txtValueEnergy, txtFinal, txtNumberLevel,txtReward;
    public Slider SliderHealthPlayer, SliderHealthEnemy;

    public Text[] txtPrice;
    public Button[] btnSelect;
    public GameObject[] okInfo;

    public GameObject pnlPreStart, pnlFinal, reward;
    public Button btnStart, btnNext;
    private int energyValue, keySelect;

    Animator animator;
    public GameObject Player, Enemy;
    // Start is called before the first frame update
    void Start()
    {
        Player.gameObject.SetActive(false);
        Enemy.gameObject.SetActive(false);
        for (int i = 0; i < okInfo.Length; i++)
        {
            okInfo[i].gameObject.SetActive(false);
        }
        btnStart.GetComponent<Button>().interactable = false;
        txtNumberLevel.text = Global.levelTowerValue.ToString();
        pnlFinal.gameObject.SetActive(false);
        pnlPreStart.gameObject.SetActive(true);
        energyValue = Global.valueClick;
        txtValueEnergy.text = energyValue.ToString();
        checkEnergy();
    }

    // Update is called once per frame
    void Update()
    {
        txtValueEnergy.text = energyValue.ToString();
        checkEnergy();
        if (Global.valueClick >= Convert.ToInt32(txtPrice[0].text) && Global.valueClick < Convert.ToInt32(txtPrice[1].text))
        {
            btnSelect[0].GetComponent<Button>().interactable = true;
            for (int i = 1; i < btnSelect.Length; i++)
            {
                btnSelect[i].GetComponent<Button>().interactable = false;
            }
        }
        else if (Global.valueClick >= Convert.ToInt32(txtPrice[1].text) && Global.valueClick < Convert.ToInt32(txtPrice[2].text))
        {
            btnSelect[0].GetComponent<Button>().interactable = true;
            btnSelect[1].GetComponent<Button>().interactable = true;
            for (int i = 2; i < btnSelect.Length; i++)
            {
                btnSelect[i].GetComponent<Button>().interactable = false;
            }
        }
        else if (Global.valueClick >= Convert.ToInt32(txtPrice[2].text) && Global.valueClick < Convert.ToInt32(txtPrice[3].text))
        {

            btnSelect[0].GetComponent<Button>().interactable = true;
            btnSelect[1].GetComponent<Button>().interactable = true;
            btnSelect[2].GetComponent<Button>().interactable = true;
            btnSelect[3].GetComponent<Button>().interactable = false;
        }
        else if (Global.valueClick >= Convert.ToInt32(txtPrice[3].text))
        {

            btnSelect[0].GetComponent<Button>().interactable = true;
            btnSelect[1].GetComponent<Button>().interactable = true;
            btnSelect[2].GetComponent<Button>().interactable = true;
            btnSelect[3].GetComponent<Button>().interactable = true;
        }
    }

    public void SelectСhance(int key)
    {
        keySelect = key;
        btnStart.GetComponent<Button>().interactable = true;
        if (Global.valueClick>= Convert.ToInt32(txtPrice[0].text))
        {
            if (key == 0 && Global.valueClick >= Convert.ToInt32(txtPrice[0].text))
            {
                energyValue = Global.valueClick - Convert.ToInt32(txtPrice[0].text);
                btnSelect[0].GetComponent<Button>().enabled = false;
                btnSelect[1].GetComponent<Button>().enabled = true;
                btnSelect[2].GetComponent<Button>().enabled = true;
                btnSelect[3].GetComponent<Button>().enabled = true;
                okInfo[0].gameObject.SetActive(true);
                okInfo[1].gameObject.SetActive(false);
                okInfo[2].gameObject.SetActive(false);
                okInfo[3].gameObject.SetActive(false);
            }
            if (key == 1 && Global.valueClick >= Convert.ToInt32(txtPrice[1].text))
            {
                energyValue = Global.valueClick - Convert.ToInt32(txtPrice[1].text);
                btnSelect[1].GetComponent<Button>().enabled = false;
                btnSelect[0].GetComponent<Button>().enabled = true;
                btnSelect[2].GetComponent<Button>().enabled = true;
                btnSelect[3].GetComponent<Button>().enabled = true;
                okInfo[0].gameObject.SetActive(false);
                okInfo[1].gameObject.SetActive(true);
                okInfo[2].gameObject.SetActive(false);
                okInfo[3].gameObject.SetActive(false);
            }
            if (key == 2 && Global.valueClick >= Convert.ToInt32(txtPrice[2].text))
            {
                energyValue = Global.valueClick - Convert.ToInt32(txtPrice[2].text);
                btnSelect[2].GetComponent<Button>().enabled = false;
                btnSelect[1].GetComponent<Button>().enabled = true;
                btnSelect[0].GetComponent<Button>().enabled = true;
                btnSelect[3].GetComponent<Button>().enabled = true;
                okInfo[0].gameObject.SetActive(false);
                okInfo[1].gameObject.SetActive(false);
                okInfo[2].gameObject.SetActive(true);
                okInfo[3].gameObject.SetActive(false);
            }
            if (key == 3 && Global.valueClick >= Convert.ToInt32(txtPrice[3].text))
            {
                energyValue = Global.valueClick - Convert.ToInt32(txtPrice[3].text);
                btnSelect[3].GetComponent<Button>().enabled = false;
                btnSelect[1].GetComponent<Button>().enabled = true;
                btnSelect[2].GetComponent<Button>().enabled = true;
                btnSelect[0].GetComponent<Button>().enabled = true;
                okInfo[0].gameObject.SetActive(false);
                okInfo[1].gameObject.SetActive(false);
                okInfo[2].gameObject.SetActive(false);
                okInfo[3].gameObject.SetActive(true);
            }
        }
        else
        {
            for (int i = 0; i < btnSelect.Length; i++)
            {
                btnSelect[i].GetComponent<Button>().interactable = false;
            }
        }
        
    }

    public void btnExit()
    {
        if (txtFinal.text == "Победа")
        {
            Global.levelTowerValue += 1;
        }
        SceneManager.LoadScene(0);
        energyValue = Global.valueClick;  
    }

    public void btnPlay()
    {
        Global.valueClick = energyValue;
        pnlPreStart.gameObject.SetActive(false);
        Player.gameObject.SetActive(true);
        Enemy.gameObject.SetActive(true);
        BattleFunc();
    }

    public void checkEnergy()
    {
        if (Global.valueClick < Convert.ToInt32(txtPrice[0].text))
        {
            for (int i = 0; i < btnSelect.Length; i++)
            {
                btnSelect[i].GetComponent<Button>().interactable = false;
            }
            btnStart.GetComponent<Button>().interactable = false;
        }

    }
    public void BattleFunc()
    {
        btnNext.gameObject.SetActive(false);
        reward.gameObject.SetActive(false);
        if (keySelect == 0)
        {
            int randValue = UnityEngine.Random.Range(0, 4);
            if (randValue == 0)
            {
                SliderHealthPlayer.GetComponent<Slider>().value = 100;
                SliderHealthEnemy.GetComponent<Animator>().enabled = true;
                txtFinal.text = "Победа";
            }
            else
            {
                SliderHealthPlayer.GetComponent<Animator>().enabled = true;
                SliderHealthEnemy.GetComponent<Slider>().value = 100;
                txtFinal.text = "Поражение";
            }
        }
        if (keySelect == 1)
        {
            int randValue = UnityEngine.Random.Range(0, 2);
            if (randValue == 0)
            {
                SliderHealthPlayer.GetComponent<Slider>().value = 100;
                SliderHealthEnemy.GetComponent<Animator>().enabled = true;
                txtFinal.text = "Победа";
            }
            else
            {
                SliderHealthPlayer.GetComponent<Animator>().enabled = true;
                SliderHealthEnemy.GetComponent<Slider>().value = 100;
                txtFinal.text = "Поражение";
            }
        }
        if (keySelect == 2)
        {
            int randValue = UnityEngine.Random.Range(0, 3);
            if (randValue != 0)
            {
                SliderHealthPlayer.GetComponent<Slider>().value = 100;
                SliderHealthEnemy.GetComponent<Animator>().enabled = true;
                txtFinal.text = "Победа";
            }
            else
            {
                SliderHealthPlayer.GetComponent<Animator>().enabled = true;
                SliderHealthEnemy.GetComponent<Slider>().value = 100;
                txtFinal.text = "Поражение";
            }
        }
        if (keySelect == 3)
        {
            int randValue = UnityEngine.Random.Range(0, 1);
            if (randValue == 0)
            {
                SliderHealthPlayer.GetComponent<Slider>().value = 100;
                SliderHealthEnemy.GetComponent<Animator>().enabled = true;
                txtFinal.text = "Победа";
            }
            else
            {
                SliderHealthPlayer.GetComponent<Animator>().enabled = true;
                SliderHealthEnemy.GetComponent<Slider>().value = 100;
                txtFinal.text = "Поражение";
            }
        }
        if (txtFinal.text == "Победа")
        {
            Win();
            btnNext.gameObject.SetActive(true);
            reward.gameObject.SetActive(true);
            for (int i = 0; i < Global.levels.Length; i++)
            {
                if (Global.levelTowerValue == i)
                {
                    Global.levels[i++] = 1;
                }
            }
            if (Global.levelTowerValue < 10)
            {
                txtReward.text = "200";
                Global.valueClick += 200;
            }
            else if (Global.levelTowerValue >= 10 && Global.levelTowerValue < 20)
            {
                txtReward.text = "500";
                Global.valueClick += 500;
            }
            else if (Global.levelTowerValue >= 20)
            {
                txtReward.text = "1000";
                Global.valueClick += 1000;
            }
        }
        else
        {
            Lose();
        }
        pnlFinal.gameObject.SetActive(true);

    }

    public void btnRestart()
    {
        SceneManager.LoadScene(1);
        energyValue = Global.valueClick;
    }
    public void Next()
    {
        Global.levelTowerValue += 1;
        energyValue = Global.valueClick;
        SceneManager.LoadScene(1);
    }

    public void Win()
    {
        animator = Player.GetComponent<Animator>();
        animator.runtimeAnimatorController = Resources.Load("Animators/Player_win") as RuntimeAnimatorController;
        animator = Enemy.GetComponent<Animator>();
        animator.runtimeAnimatorController = Resources.Load("Animators/Enemy_lose") as RuntimeAnimatorController;
    }
    public void Lose()
    {
        animator = Player.GetComponent<Animator>();
        animator.runtimeAnimatorController = Resources.Load("Animators/Player_lose") as RuntimeAnimatorController;
        animator = Enemy.GetComponent<Animator>();
        animator.runtimeAnimatorController = Resources.Load("Animators/Enemy_win") as RuntimeAnimatorController;
    }
}
